#include<stdio.h>
#include<unistd.h>

int main(int argc,char *argv[])
{
	char *delivery = "";
	int thick =0;
	int extra_cheese=0;
	int count=0;
	char ch;
	while((ch=getopt(argc,argv,"d:te"))!=EOF)
		switch(ch){
			case 'd':
		 	 delivery=optarg;
			 break;
			case 't':
			 thick=1;
			 break;
			case 'e':
			 extra_cheese=1;
			 break;
			default:
			 fprintf(stderr,"Unknown option:'%s'\n",optarg);
			 return 1;
		}
	argc-=optind;
	argv+=optind;
	if(thick)
		puts("Thick Crust");
	if(extra_cheese)
		puts("Extra Cheese");
	if(delivery[0])
		printf("To be delivered %s.\n",delivery);
	puts("Ingredients:");
	for(count=0;count<argc;count++)
		puts(argv[count]);
	return 0;
}
